# fetloader.py
#
# MIT License
# 
# Copyright (c) 2021 zrn1x (Arsen O.) 
# Copyright (c) 2021 clownless (Maxim K.)
# Copyright (c) 2021 pasted1337
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import configparser
import os
import pkg_resources
import sys
import time

FAIL = '\033[91m'
RESET = "\033[0m"


figlet = """  __     _   _              _                    
 / _|___| |_| |___  __ _ __| |___ _ _  _ __ _  _ 
|  _/ -_)  _| / _ \\/ _` / _` / -_) '_|| '_ \\ || |
|_| \\___|\\__|_\\___/\\__,_\\__,_\\___|_|(_) .__/\\_, |
                                      |_|   |__/ 
 b5 - hotfix 02 - yougame.biz - made by z3r0nyaa
"""
print(figlet)
print("-- checking for fetloader folder", end="\r")
hh = os.path.exists("C:\\fetloader.py\\")
ff = os.path.exists("C:\\fetloader.py\\config.ini")
rr = os.path.exists("C:\\fetloader.py\\aye1337nocap.py")
sys.path.append("C:\\fetloader.py\\")
injectorlink = "https://raw.githubusercontent.com/numaru/injector/master/injector.py"

print("-- checking for pywin32         ", end="\r")
installed_packages      = pkg_resources.working_set
installed_packages_list = sorted(["%s==%s" % (i.key, i.version)
     for i in installed_packages])
pkgs = [i.split('==', 1)[0] for i in installed_packages_list]
pywin32installs = pkgs.count("pywin32")

if pywin32installs == 1:
	from win32com.client import GetObject
else:
	os.system("pip install pywin32")
	os.system("pip install pypiwin32")
	from win32com.client import GetObject

print("-- checking for requests        ", end="\r")
requestsinstalls = pkgs.count("requests")
if requestsinstalls == 1:
	import requests
else:
	os.system("pip install requests")
	import requests

print("-- module checks done           ", end="\r")

basecfg = """[fetloader]
dllproject = 25080350
branch = main"""

print("-- checking for main folder     ", end="\r")
if hh == False:
	os.system("mkdir C:\\fetloader.py\\")

print("-- checking for config          ", end="\r")
if ff == False:
	ddd = open("C:\\fetloader.py\\config.ini", "a+")
	ddd.write(basecfg)

print("-- checking for injector library", end="\r")
if rr == False:
	print("-- downloading injector library ", end="\r")
	r = requests.get(injectorlink, allow_redirects=True)
	open("C:\\fetloader.py\\aye1337nocap.py", 'wb').write(r.content)
	from aye1337nocap import Injector
	injector = Injector()
else:
	from aye1337nocap import Injector
	injector = Injector()

print("-- initializing config          ", end="\r")
config    = configparser.ConfigParser()
config.read("C:\\fetloader.py\\config.ini")
cheatbranch = config['fetloader']['branch']
try:
	cheatrepo = config['fetloader']['dllproject']
except(KeyError):
	print("-- config outdated, recreating  ", end="\r")
	basecfg = """[fetloader]
dllproject = 25080350
branch = main"""
	os.remove("C:\\fetloader.py\\config.ini")
	open("C:\\fetloader.py\\config.ini", "w").write(basecfg)
	cheatrepo = config['fetloader']['dllproject']

print("-- removing cheats.ini          ", end="\r")
try:
	os.remove("C:\\fetloader.py\\cheats.ini")
except(FileNotFoundError):
	pass

cheatrepolink = "https://gitlab.com/api/v4/projects/" + cheatrepo + "/repository/files/cheats.ini/raw?ref=" + cheatbranch
r = requests.get(cheatrepolink, allow_redirects=True)
open('C:\\fetloader.py\\cheats.ini', 'wb').write(r.content)

aye = open("C:\\fetloader.py\\cheats.ini").read()
fet = aye.split("[inject]")[0]
open("C:\\fetloader.py\\cheats.ini", "w").write(fet)
print("-- downloaded cheats.ini        ", end="\r")

config.read("C:\\fetloader.py\\cheats.ini")
cheatsitems = config.items("cheats")

print("-- choose an option             ")

mydict = {}
print("vac - load vac-bypass")

# variables are shitty but idfc
i = 0
for a in cheatsitems:
	d  = list(a)
	c  = list(a)
	i  = i + 1
	aw = str(i)
	mydict['cs' + aw] = d.pop(1)
	print("cs" + aw, sep=' ', end='', flush=True)
	print(" - ", sep=' ', end='', flush=True)
	print(c.pop(0), sep=' ', end='', flush=True)
	print(" - ", sep=' ', end='', flush=True)
	print(mydict['cs' + aw], sep=' ', end='', flush=True)
	print("\n", sep=' ', end='', flush=True)

print("> ", sep=' ', end='', flush=True)
cheatload = input()

if cheatload == "vac":
	r = requests.get("https://gitlab.com/api/v4/projects/25080350/repository/files/vac-bypass.exe/raw?ref=main", allow_redirects=True)
	open("C:\\fetloader.py\\vac-bypass.exe", "wb").write(r.content)
	print("-- downloading vac-bypass.exe", end="\r")
	os.system("C:\\fetloader.py\\vac-bypass.exe")
	print("-- started vac-bypass.exe    ")
	time.sleep(5)
else:
	print("-- downloading emb.exe        ", end="\r")
	r = requests.get("https://gitlab.com/api/v4/projects/25080350/repository/files/emb.exe/raw?ref=main", allow_redirects=True)
	open("C:\\fetloader.py\\emb.exe", "wb").write(r.content)
	print("-- started emb.exe            ", end="\r")
	os.system("C:\\fetloader.py\\emb.exe")
	print("-- getting csgo.exe pid       ", end="\r")
	WMI = GetObject('winmgmts:')
	processes = WMI.InstancesOf('Win32_Process')
	process_list = [(p.Properties_("Name").Value, p.Properties_("ProcessID").Value) for p in processes]
	ay = [t for t in process_list if t[0].startswith('csgo.exe')]
	try:
		ad = ay[0]
	except(IndexError):
		print(FAIL + "-- csgo.exe is not started lol" + RESET)
		time.sleep(5)
		exit(1)
	ae = list(ad)
	ae.pop(0)
	aq = ae[0]
	ar = str(aq)
	print("-- csgo.exe pid " + ar + "  ", end="\r")
	linkdll = str("https://gitlab.com/api/v4/projects/" + cheatrepo + "/repository/files/" + mydict[cheatload] + "/raw?ref=main")
	print("-- downloading " + linkdll)
	r = requests.get(linkdll, allow_redirects=True)
	dllpath = str("C:\\fetloader.py\\" + mydict[cheatload])
	open("C:\\fetloader.py\\" + mydict[cheatload], "wb").write(r.content)
	print("-- injecting " + mydict[cheatload] + "    ", end="\r")
	injector.load_from_pid(aq)
	injector.inject_dll(dllpath)
	print("-- injected! now you can close this window")
	time.sleep(5)

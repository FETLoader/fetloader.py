<br />
<p align="center">
  <a href="https://fetloader.xyz">
    <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/25341870/fetpy.png" alt="Logo" height="128">
  </a>



### [FET Loader](https://gitlab.com/FETLoader/FETLoader) python3 recode

## Features
- Big cheats library (no)
- Opensource
- A more shitcode

## How to use
- Go to tags
- Select latest tag
- Download exe or py
- Double-click it

## Building .exe
!!! You can build .exe only under Windows installation! If you try building in Linux, you will get default Linux binary file.
- Clone repo
- Go to build folder
- Start build.bat

## Contributing
Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## License
Distributed under the MIT License. See `LICENSE` for more information.

## Credits
- Loader: [zrn1x](https://zeronyaa.tk)
- ExternalModuleBypasser: [0x000cb](https://github.com/0x000cb)
- VAC-Bypass-Loader: [danielkrupinski](https://github.com/danielkrupinski/VAC-Bypass-Loader)
- Big thanks to MORGENSHTERN and his tracks
- Big thanks to h1nt and his awesome HvH streams
- Big thanks to Xiaomi and their awesome Redmi Note 8
- Big thanks to John and his awesome rusherhack
